# Freshmen:
Read this. It will be highly informative for you. https://gitlab.mpcdf.mpg.de/amerlo/phd-onboarding/

# W7-X Data:

* [W7-X Archive](http://archive-webapi.ipp-hgw.mpg.de)
    * Use the high level [W7X_API](https://gitlab.mpcdf.mpg.de/kjbrunne/W7X_APIpy) package for extracting data from the archive. A good start with the package can be found [in this jupyter notebook](https://gitlab.mpcdf.mpg.de/kjbrunne/W7X_APIpy/-/blob/master/doc/workshop.ipynb).
    * More low level APIs are
        * [W7xdia](https://git.ipp-hgw.mpg.de/boz/w7xdia/)
        * [W7xdiags](https://gitlab.mpcdf.mpg.de/kjbrunne/w7xdiags)
* [W7-X Logbook](http://logbook.ipp-hgw.mpg.de)
    * Hover over the small blue question mark on the left to get the tool tips for using the search    
        * For example: for looking up a specific session ID you'd search for tags.SID:S## where ## is replaced with the session number.
* [W7-X Datamonitor](http://datamonitor.ipp-hgw.mpg.de)
* 

